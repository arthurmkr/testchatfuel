package org.home.test.chatfuel.listener;

public interface PersonListener {
    PersonListener EMPTY = new PersonListener() {
        @Override
        public void onLeaveFromLift() {

        }

        @Override
        public void onEnterToLift() {

        }
    };

    void onLeaveFromLift();

    void onEnterToLift();
}
