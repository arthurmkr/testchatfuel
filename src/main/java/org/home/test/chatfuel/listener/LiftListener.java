package org.home.test.chatfuel.listener;

public interface LiftListener {
    LiftListener EMPTY = new LiftListener() {
        @Override
        public void onUpdateCurrentFloor(int curFloor) {
        }

        @Override
        public void onDoorsOpen() {
        }

        @Override
        public void onDoorsClose() {
        }
    };

    void onUpdateCurrentFloor(int curFloor);

    void onDoorsOpen();

    void onDoorsClose();
}
