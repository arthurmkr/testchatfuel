package org.home.test.chatfuel;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.home.test.chatfuel.components.LiftView;
import org.home.test.chatfuel.model.Lift;
import org.home.test.chatfuel.model.Person;

import java.io.IOException;

public class LiftApplication extends Application {
    private Scene mainScene;
    private Scene inputScene;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        Scene inputScene = createInputScene((maxFloor, height, speed, doorsTime) -> {
            Lift lift = new Lift(height, maxFloor, speed, doorsTime);
            Person person = new Person();

            LiftView liftView = new LiftView(lift, person);
            mainScene = new Scene(liftView.getControl(), 200 + 35*maxFloor, 370);
            stage.setScene(mainScene);
            stage.show();
        });

        stage.setTitle("Симулятор лифта");
        stage.setScene(inputScene);
        stage.setResizable(false);

        stage.show();
    }

    private Scene createInputScene(OnStartHandler handler) {
        GridPane inputs = new GridPane();
        inputs.setVgap(10);

        inputs.add(new Label("Кол-во этажей в подъезде"), 0, 0);
        inputs.add(new Label("Высота одного этажа (м)"), 0, 1);
        inputs.add(new Label("Скорость лифта (м/сек)"), 0, 2);

        Label child = new Label("Время между открытием и закрытием дверей(сек)");
        child.setMaxWidth(200);
        child.setWrapText(true);
        inputs.add(child, 0, 3);

        Spinner<Integer> floorCountInput = new Spinner<>(5, 20, 10);
        floorCountInput.setMaxWidth(80);
        inputs.add(floorCountInput, 1, 0);

        Spinner<Integer> floorHeightInput = new Spinner<>(1, 10, 3);
        floorHeightInput.setMaxWidth(80);
        inputs.add(floorHeightInput, 1, 1);

        Spinner<Integer> speedInput = new Spinner<>(1, 10, 3);
        speedInput.setMaxWidth(80);
        inputs.add(speedInput, 1, 2);

        Spinner<Integer> doorsTimeInput = new Spinner<>(1, 20, 10);
        doorsTimeInput.setMaxWidth(80);
        inputs.add(doorsTimeInput, 1, 3);

        Button button = new Button("Старт");
        button.setOnAction(event -> handler.handle(
                floorCountInput.getValue(),
                floorHeightInput.getValue(),
                speedInput.getValue(),
                doorsTimeInput.getValue()));

        VBox vBox = new VBox();
        vBox.setPadding(new Insets(10));
        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().add(inputs);
        vBox.getChildren().add(button);

        return new Scene(vBox, 290, 190);
    }

    @FunctionalInterface
    interface OnStartHandler {
        void handle(int maxFloor, int height, int speed, int doorsTime);
    }
}


