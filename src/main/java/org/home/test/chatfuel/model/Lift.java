package org.home.test.chatfuel.model;

import org.home.test.chatfuel.listener.LiftListener;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Lift {
    private static final int FREQUENCY = 10;
    private final BlockingQueue<Integer> events = new LinkedBlockingQueue<>(1);
    private LiftListener listener = LiftListener.EMPTY;
    private int curFloor = 1;
    private int maxFloor;

    public Lift(int floorHeight, int maxFloor, int liftSpeed, int timeOfOpenedDoors) {
        this.maxFloor = maxFloor;

        Thread thread = new Thread(() -> {
            try {
                int gotoFloor;
                float speed = (float)liftSpeed / FREQUENCY;
                while ((gotoFloor = events.take()) > 0) {
                    float curVerticalPosition = (curFloor - 1) * floorHeight;
                    float koeff = 0;

                    if (gotoFloor > curFloor) {
                        koeff = 1;
                    } else if (gotoFloor < curFloor) {
                        koeff = -1;
                    }

                    while (curFloor != gotoFloor) {
                        Thread.sleep(1000 / FREQUENCY);

                        curVerticalPosition = Math.max(0, curVerticalPosition + koeff * speed);
                        curFloor = 1 + (int) Math.min((float) maxFloor, curVerticalPosition / floorHeight);
                        // refresh view
                        listener.onUpdateCurrentFloor(curFloor);
                    }

                    // open doors
                    listener.onDoorsOpen();

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            listener.onDoorsClose();
                        }
                    }, timeOfOpenedDoors * 1000);
                }


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        thread.setDaemon(true);

        thread.start();
    }

    public void goToFloor(int floor) {
        events.offer(floor);
    }


    public void setListener(LiftListener listener) {
        this.listener = Objects.requireNonNull(listener);
    }

    public int getMaxFloor() {
        return maxFloor;
    }
}
