package org.home.test.chatfuel.model;

import org.home.test.chatfuel.listener.PersonListener;

import java.util.Objects;


public class Person {
    private PersonListener listener = PersonListener.EMPTY;
    private boolean insideLift;

    public void setListener(PersonListener listener) {
        this.listener = Objects.requireNonNull(listener);
    }

    public boolean isInsideLift() {
        return insideLift;
    }

    public void leaveFromLift() {
        insideLift = false;

        listener.onLeaveFromLift();
    }

    public void enterToLift() {
        insideLift = true;

        listener.onEnterToLift();
    }
}
