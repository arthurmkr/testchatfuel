package org.home.test.chatfuel.components;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

public class CurrentLiftPositionPanelView {
    private HBox hBox;
    private List<BorderPane> floors = new ArrayList<>();
    private int prevFloor;

    public CurrentLiftPositionPanelView(long maxFloor) {
        hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setAlignment(Pos.CENTER_LEFT);

        ObservableList<Node> children = hBox.getChildren();
        Text e = new Text("Текущий этаж: ");
        e.setFont(new Font(20));
        e.setWrappingWidth(150);

        children.add(e);

        for (int i = 1; i <= maxFloor; i++) {
            BorderPane floorPane = new BorderPane();

            Text floor = new Text("" + i);

            floorPane.setPrefWidth(30);

            floors.add(floorPane);

            floorPane.setCenter(floor);
            hBox.getChildren().add(floorPane);
        }
    }

    public void setCurrentFloor(int floor) {
        if (prevFloor == floor) {
            return;
        }

        if (prevFloor != 0) {
            floors.get(prevFloor - 1).setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        }


        if (floor >= 1 && floor <= floors.size()) {
            floors.get(floor - 1).setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
            prevFloor = floor;
        } else {
            prevFloor = 0;
        }
    }

    public Node getControl() {
        return hBox;
    }
}
