package org.home.test.chatfuel.components;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.home.test.chatfuel.listener.LiftListener;
import org.home.test.chatfuel.listener.PersonListener;
import org.home.test.chatfuel.model.Lift;
import org.home.test.chatfuel.model.Person;

public class LiftView implements LiftListener, PersonListener {
    private Lift lift;
    private Person person;

    private DoorsView doors;
    private PersonView personView;
    private CurrentLiftPositionPanelView currentLiftPositionPanel;
    private VBox root;
    private PersonInOutPanelView personPanel;
    private FloorNumberPanel outsidePanel;
    private FloorNumberPanel insidePanel;


    public LiftView(Lift lift, Person person) {
        lift.setListener(this);
        person.setListener(this);
        this.lift = lift;
        this.person = person;

        root = new VBox();
        root.setPadding(new Insets(20));
        root.setSpacing(15);

        ObservableList<Node> children = root.getChildren();

        doors = new DoorsView();
        children.add(doors.getControl());

        currentLiftPositionPanel = new CurrentLiftPositionPanelView(lift.getMaxFloor());
        children.add(currentLiftPositionPanel.getControl());

        outsidePanel = new FloorNumberPanel("Вызов лифта на этаж: ", lift);
        children.add(outsidePanel.getControl());

        personPanel = new PersonInOutPanelView(person);
        children.add(personPanel.getControl());

        personView = new PersonView();
        children.add(personView.getControl());

        insidePanel = new FloorNumberPanel("Кнопки этажа внутри лифта: ", lift);
        Node control = insidePanel.getControl();
        control.setDisable(true);
        children.add(control);
    }


    @Override
    public void onUpdateCurrentFloor(int curFloor) {
        currentLiftPositionPanel.setCurrentFloor(curFloor);
    }

    @Override
    public void onDoorsOpen() {
        doors.open();
        if (person.isInsideLift()) {
            personPanel.setDisableButtonLeave(false);
        } else {
            personPanel.setDisableButtonEnter(false);
        }
    }

    @Override
    public void onDoorsClose() {
        doors.close();
        personPanel.setDisableButtonEnter(true);
        personPanel.setDisableButtonLeave(true);

        if (person.isInsideLift()) {
            insidePanel.getControl().setDisable(false);
        } else {
            outsidePanel.getControl().setDisable(false);
        }
    }

    public Pane getControl() {
        return root;
    }

    @Override
    public void onLeaveFromLift() {
        personPanel.setDisableButtonLeave(true);
        personPanel.setDisableButtonEnter(false);

        personView.outside();
    }

    @Override
    public void onEnterToLift() {
        personPanel.setDisableButtonEnter(true);
        personPanel.setDisableButtonLeave(false);

        personView.inside();
    }
}
