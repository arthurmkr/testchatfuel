package org.home.test.chatfuel.components;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import org.home.test.chatfuel.model.Lift;

public class FloorNumberPanel {
    private HBox hBox;

    public FloorNumberPanel(String title, Lift lift) {
        hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setAlignment(Pos.CENTER_LEFT);

        Text e = new Text(title);
        e.setFont(new Font(20));
        e.setWrappingWidth(150);

        hBox.getChildren().add(e);

        for (int i = 1; i <= lift.getMaxFloor(); i++) {
            Button button = new Button();
            button.setText("" + i);
            button.setPrefWidth(30);

            int finalI = i;
            button.setOnAction(event -> {
                hBox.setDisable(true);

                lift.goToFloor(finalI);
            });

            hBox.getChildren().add(button);
        }
    }

    public Node getControl() {
        return hBox;
    }
}
