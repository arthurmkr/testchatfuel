package org.home.test.chatfuel.components;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import org.home.test.chatfuel.model.Person;

public class PersonInOutPanelView {
    private HBox hBox;
    private Button enter;
    private Button leave;

    public PersonInOutPanelView(Person person) {
        hBox = new HBox();

        hBox.setSpacing(15);
        ObservableList<Node> children = hBox.getChildren();

        enter = new Button("Войти в лифт");
        leave = new Button("Выйти из лифта");

        enter.setDisable(true);
        enter.setOnAction(event -> person.enterToLift());
        children.add(enter);

        leave.setDisable(true);
        leave.setOnAction(event -> person.leaveFromLift());
        children.add(leave);
    }

    public Node getControl() {
        return hBox;
    }

    public void setDisableButtonEnter(boolean state) {
        enter.setDisable(state);
    }

    public void setDisableButtonLeave(boolean state) {
        leave.setDisable(state);
    }
}
