package org.home.test.chatfuel.components;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class PersonView {
    private static final String OUTSIDE = "снаружи";
    private static final String INSIDE = "внутри";

    private Text personStatus;
    private HBox hBox;

    public PersonView() {
        hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setAlignment(Pos.CENTER_LEFT);

        ObservableList<Node> children = hBox.getChildren();
        Text e = new Text("Статус пользователя: ");
        e.setFont(new Font(20));
        e.setWrappingWidth(150);

        children.add(e);

        personStatus = new Text(OUTSIDE);
        personStatus.setFont(new Font(20));
        personStatus.setWrappingWidth(150);
        children.add(personStatus);

        outside();
    }

    public void outside() {
        personStatus.setText(OUTSIDE);
    }

    public void inside() {
        personStatus.setText(INSIDE);
    }

    public Node getControl() {
        return hBox;
    }
}
