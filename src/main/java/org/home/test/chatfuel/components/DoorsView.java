package org.home.test.chatfuel.components;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class DoorsView {
    private static final String OPENED = "открыты";
    private static final String CLOSED = "закрыты";

    private Text doorsStatus;
    private HBox hBox;

    public DoorsView() {
        hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setAlignment(Pos.CENTER_LEFT);

        ObservableList<Node> children = hBox.getChildren();
        Text e = new Text("Статус дверей: ");
        e.setFont(new Font(20));
        e.setWrappingWidth(150);

        children.add(e);

        doorsStatus = new Text(OPENED);
        doorsStatus.setFont(new Font(20));
        doorsStatus.setWrappingWidth(150);
        children.add(doorsStatus);

        close();
    }

    public void open() {
        doorsStatus.setText(OPENED);
    }

    public void close() {
        doorsStatus.setText(CLOSED);
    }

    public Node getControl() {
        return hBox;
    }
}
